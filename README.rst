FunHPC: Functional HPC Programming
==================================

This is FunHPC.cxx, a C++ library that provides fine-grained
multi-threading for distributed-memory systems in a functional
programming style.

Comments and contributions are welcome.
